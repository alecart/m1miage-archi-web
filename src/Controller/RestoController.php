<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Resto;

class RestoController extends AbstractController
{
    public function ajouter($nom, $chef, $etoiles)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $resto = new Resto;
        $resto->setNom($nom);
        $resto->setChef($chef);
        $resto->setEtoiles($etoiles);
        $entityManager->persist($resto);
        $entityManager->flush();
        return $this->redirectToRoute(
            'resto_voir',
            array('id' => $resto->getId())
        );
    }

    public function ajout(Request $request)
    {
        $resto = new Resto;
        $form = $this->createFormBuilder($resto)
            ->add('nom', TextType::class)
            ->add('chef', TextType::class)
            ->add('etoiles', IntegerType::class)
            ->add('envoyer', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resto);
            $entityManager->flush();
            return $this->redirectToRoute(
                'resto_voir',
                array('id' => $resto->getId())
            );
        }
        return $this->render(
            'resto/ajout.html.twig',
            array('monFormulaire' => $form->createView())
        );
    }

    public function voir($id)
    {
        $resto = $this->getDoctrine()->getRepository(resto::class)->find($id);
        if (!$resto)
            throw $this->createNotFoundException('resto[id=' . $id . '] inexistante');
        return $this->render('resto/voir.html.twig', array('resto' => $resto));
    }

    public function menu()
    {
        return $this->render('resto/menu.html.twig');
    }
}
